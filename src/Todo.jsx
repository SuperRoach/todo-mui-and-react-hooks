import * as React from 'react';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import './Todo.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { useInputValue, useTodos } from "./hooks";
import TodoList from './components/TodoList';
import AddTodo from './components/AddTodo';
import StatusBar from './components/Statusbar'
import FilterBoxOptions from './components/FilterBox'

<link
  rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
/>

const Todo = React.memo(() => {
  const { inputValue, changeInput, clearInput, keyInput } = useInputValue();
  const { filteredTodos, filter, addTodo, completeTodo, removeTodo, prioritiseTodo, defaultTodo, filterTodo } = useTodos();

  const handleAddTodo = () => {
    clearInput();
    addTodo(inputValue);
    filterTodo(null, filter);
  };

  return (
    <Container>
      <header className="App-header">
        <h1>Todo List challenge.</h1>
      </header>
      <AddTodo
        inputValue={inputValue}
        onInputChange={changeInput}
        onButtonClick={handleAddTodo}
        onInputKeyPress={(event) => keyInput(event, handleAddTodo)}
      />
      <FilterBoxOptions
        filterChange={filterTodo}
        filterValue={filter}
      />
      <TodoList
        items={filteredTodos}
        onItemComplete={completeTodo}
        onItemRemove={removeTodo}
        onItemPrioritise={prioritiseTodo}
      />
      {/* For global instead of local list counts, swap between Todos and filteredTodos */}
      <StatusBar items={filteredTodos} />

      <h4>Speed things up?</h4>  
      <Button variant="contained" onClick={defaultTodo}>Set placeholder todos</Button>
    </Container>
  );
});


export default Todo;