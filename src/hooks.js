import { useState, useEffect } from "react";

// This file could have also been called Business logic. By breaking away model or 
// core function from the components themselves, I allow for re-usable code, that can
// be ported to different visual layouts or apps easily.


const todoDefaults = [
  { text: "Pick the coffee up", checked: false, priority: true },
  { text: "Choose favourite colour of the day", checked: true, priority: false },
  { text: "Sandwich additions", checked: false, priority: true },
  { text: "Prelaminating Drywall Inventory", checked: false, priority: false },
  { text: "Blurring Reality Lines", checked: false, priority: false },
  { text: "Limiting Litterbox Loads", checked: false, priority: false },
  { text: "Assessing Loam Particle Sizes", checked: true, priority: true },
  { text: "Unfolding Helix Packet", checked: true, priority: true },
]


// Search bar handler
export const useInputValue = (initialValue = "") => {
  const [inputValue, setInputValue] = useState(initialValue);

  return {
    inputValue,
    changeInput: (event) => setInputValue(event.target.value),
    clearInput: () => setInputValue(""),
    keyInput: (event, callback) => {
      // Enter Key
      if (event.which === 13 || event.keyCode === 13) {
        callback(inputValue);
        return true;
      }

      return false;
    }
  };
};


// Todo List Modification/CRUD Handling
export const useTodos = (initialValue = []) => {
  const [todos, setTodos] = useState(initialValue);
  const [filteredTodos, setFilteredTodos] = useState(initialValue);
  const [filter, setFilter] = useState(() => []);

  // Abstracted the core filtering logic out, to be easier to modify and test outside of the app.
  const filterLogic = (workingTodo, filter) => {
    // Because we are working with a immutable and a mutable, we need a placeholder to store the result.
    let result = workingTodo;
    if (filter && filter.includes("priorityOnly")) {
      result = result.filter(function (result) {
        return result.priority;
      })
    }

    if (filter && filter.includes("nameSort")) {
      result.sort((a, b) => a.text.localeCompare(b.text));
    }
    return result;
  }


  // When we have modified the todo's list, reflect filtering changes (if any)
  // Inside the filteredTodo's output
  useEffect((filter) => {
    setFilteredTodos(filterLogic(todos, filter))
  }, [todos]);

  return {
    todos,
    filteredTodos,
    filter,
    addTodo: (text) => {
      // Do nothing if there is no entry yet.
      // Improvement idea: I could hoist the logic on the add button that visually shows if it can be used into here.
      if (text !== "") {
        // Noting here that because we handle default values for a new todo item, 
        // these extra parameters are not required. It does mean handling filters later is quicker though.
        // We could also add an extra user input to predefine something as priority and make below a variable too.
        setTodos(
          todos.concat({
            text,
            checked: false,
            priority: false
          })
        );
      }
      // Now that we have set the todo, we need to apply that to the filtered set.
      // filterTodo(null, filter);
    },
    completeTodo: (index) => {
      setTodos(

        // Iterate over entire list, and check the given items
        // PROS: This will handle swapping states, and ensure entire stack is consistent.
        // CONS: Not truely controlled. For a larger set of items it would be better to 
        //       filter array and work on that instead

        todos.map((todo, id) => {
          // If completeTodo is called on this ID, we want to reverse/Toggle it's given state.
          if (id === index) {
            todo.checked = !todo.checked;
          }

          return todo;
        })
      );
    },
    prioritiseTodo: (index) => {
      setTodos(
        // Iterate over entire list, and prioritise the given items
        // PROS: Moving this to a number input would be quick.
        // CONS: This could do with a refactoring of completeTodo, passing a param to determine what it should look at modifying.
        todos.map((todo, id) => {
          // If prioritiseTodo is called on this ID, we want to reverse/Toggle it's given state.
          if (id === index) {
            todo.priority = !todo.priority;
          }

          return todo;
        })
      );
    },
    removeTodo: (id) => {
      // Filter out the selected item. More efficent than pop.
      setTodos(todos.filter((_, index) => id !== index));
    },
    defaultTodo: () => {
      setTodos(todoDefaults);
    },
    filterTodo: (e, newFilter) => {
      setFilter(newFilter);
      setFilteredTodos(filterLogic(todos, newFilter));
    }

  };
};
