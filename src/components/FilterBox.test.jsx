
import { render, screen, within } from '@testing-library/react';
import FilterBox from './FilterBox';

test('Text is displayed in a list item', () => {
  render(<FilterBox />);
  const FilterText = screen.getByText(/Filter By:/i);
  expect(FilterText).toBeInTheDocument();
});

test('With no filters, passes the same data back', () => {
    render(<FilterBox items={[{text: "I'm a test", checked: false, priority: false}]} filteredOutput= />);
    const FilterText = screen.getByText(/Filter By:/i);
    expect(FilterText).toBeInTheDocument();
  });


// test('Buttons shown when no items to filter', () => {
//     render(<FilterBox />);
//     const { getAllByRole } = within(FilterBox);
//     const items = getAllByRole('button');
//     expect(items).toHaveLength(0);
//   });