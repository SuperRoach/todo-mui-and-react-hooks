import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';


const StatusBar = (props) => {
    const [todos, setTodos] = useState({ ...props.items });
    const [complete, setComplete] = useState(0);
    const [active, setActive] = useState(0);
    const [progress, setProgress] = useState(0);

    useEffect(() => {
        setTodos(props.items);
        // Option: the incomplete todo's could be inferred from complete - active, saving a variable. 
        // This is easier to read and modify though.
        setComplete(props.items.filter(t => t.checked).length);
        setActive(props.items.filter(t => !t.checked).length);
        setProgress(props.items.filter(t => t.checked).length / props.items.length * 100);

    }, [props.items]);

    return (
        <>
            <Box sx={{ width: '80%', mx: "auto" }}>
                <div>
                    {/* Improvement suggestion: to use a pluralise package to cover singular vs multiple in english - 1 todo vs 2 todos */}
                    {(todos && todos.length > 0) ? <p>There are {complete} total Todo's done. {active} to go!</p> : <p>No Todo items yet - try adding one.</p>}
                </div>
                <LinearProgress variant="determinate" value={progress} />
            </Box>
        </>
    );
};

export default StatusBar;