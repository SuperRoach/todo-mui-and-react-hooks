import ListItem from '@mui/material/ListItem';
// import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import StarBorderOutlinedIcon from '@mui/icons-material/StarBorderOutlined';
import StarOutlinedIcon from '@mui/icons-material/StarOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { ListItemSecondaryAction, IconButton, ListItemButton } from '@mui/material';


/**
 * Todo List Item - with priority, completion options
 * @constructor
 * @param {string} text - The text of the todo item
 * @param {boolean} checked - If the todo is completed or not
 * @param {boolean} divider - Visually show a divider under the todo item.
 * @param {boolean} priority - Star/Prioritise a todo item.
 * @function onCheckBoxToggle - controlled item flow for checked
 * @function onPrioritiseToggle - controlled item flow for changing the star/priority.
 * @function onDeleteClick - controlled item flow for deleting a single todo
 */
const TodoListItem = ({ text = "", checked = false, divider = true, priority = false, onCheckBoxToggle, onDeleteClick, onPrioritiseToggle }) => {
    function PriorityStatus(props) {
        const CurrentIcon = (props.priority ? <StarOutlinedIcon /> : <StarBorderOutlinedIcon />);
        return (CurrentIcon);
    }

    return (
        <ListItem divider={divider} >
            <ListItemButton onClick={onCheckBoxToggle} dense>
                <Checkbox checked={checked} />
                <ListItemText
                    primary={text}
                />
            </ListItemButton>
            <ListItemSecondaryAction>
                <IconButton aria-label="Prioritise" sx={{ mr: 1 }} onClick={onPrioritiseToggle}>
                    <PriorityStatus priority={priority} />
                </IconButton>
                <IconButton aria-label="Delete Todo" onClick={onDeleteClick}>
                    <DeleteOutlineIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
};

TodoListItem.defaultProps = {
    divider: true,
    priority: false
}

export default TodoListItem;