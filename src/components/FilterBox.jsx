import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Tooltip from '@mui/material/Tooltip';
import SortByAlphaIcon from '@mui/icons-material/SortByAlpha';
import Box from '@mui/material/Box';
import StarsIcon from '@mui/icons-material/Stars';

/**
 * Provides filtering options with feedback on the current state
 * @param {Array} {filterValue} for Controlled Input setting the visual look
 * @param {any} {filterChange} for lifting state of this component up to the parent
 * @returns {any}
 */
const FilterBoxOptions = ({ filterValue, filterChange }) => {
    return (
        <>
            <Box sx={{ width: '80%', mx: "auto" }}>
                <h3>Filter by:</h3>
                <ToggleButtonGroup
                    value={filterValue}
                    onChange={filterChange}
                    aria-label="Todo list filter"
                >
                    <ToggleButton value="nameSort" aria-label="bold">
                        <Tooltip title="Sort by Alphabetical order A-Z" describeChild>
                            <SortByAlphaIcon />
                        </Tooltip>
                    </ToggleButton>
                    <ToggleButton value="priorityOnly" aria-label="italic">
                        <Tooltip title="Toggles between showing priority todos" describeChild>
                            <StarsIcon />
                        </Tooltip>
                    </ToggleButton>
                </ToggleButtonGroup>
            </Box>
        </>
    );
}

export default FilterBoxOptions;
