import React from "react";
import { TextField, Paper, Button, Grid } from "@mui/material";

/**
 * Textfield with add button to add a new todo item.
 * @param {any} inputValue for Controlled form feedback
 * @param {any} onInputChange lifting state to parent
 * @param {any} onInputKeyPress lifting state to parent
 * @param {any} onButtonClick when user has clicked the add button.
 * @returns {any}
 */
const AddTodo = React.memo(
    ({ inputValue, onInputChange, onInputKeyPress, onButtonClick }) => (
        <Paper style={{ margin: 16, padding: 16 }}>
            <Grid container>
                <Grid xs={10} md={11} item style={{ paddingRight: 16 }}>
                    <TextField
                        placeholder="Type your Todo here"
                        // Controlled Form input due to wanting to reset the text after adding a todo.
                        value={inputValue}
                        onChange={onInputChange}
                        onKeyPress={onInputKeyPress}
                        fullWidth
                    />
                </Grid>
                <Grid xs={2} md={1} item>
                    <Button
                        fullWidth
                        sx={{ pt: 2, pb: 2 }}
                        color="primary"
                        variant={(inputValue && inputValue.length > 0) ? "contained" : "outlined" }
                        onClick={onButtonClick}
                    >
                        Add
                    </Button>
                </Grid>
            </Grid>
        </Paper>
    )
);

export default AddTodo;