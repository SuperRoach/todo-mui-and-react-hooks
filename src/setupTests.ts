// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

// This file is extremely important. Normally what I would do is when I reliant service, I would mock it up here. 
// For example a graphql backend I can mock here, so I only need to mock the result when needed.